# OverWatchStats

Simple scripts to automatically retrieve stats about the last game that was played from the official website
https://playoverwatch.com. This can be used to build a match history for statistical analysis.

## Requirements
- MongoDB
- Python3, pip
- pymongo
- beautifulsoup4

## Installation
```   
$ python3 setup.py install
```
This automatically pulls in the dependencies from PyPI.

## Usage
- Start MongoDB on the default port **27017**.
- Use `register_player` for adding new profiles:

```
$ ows register_player battletag
$ ows register_player Dude#2424
```

- Use `update_history.py` after each game to monitor changes.
- If the flag `-m`/`--map` is provided you may manually enter the name of the map that was played
(or an unique substring of it)

```
$ ows update_history
$ ows update_history -m hana
$ ows update_history --map "Volskaya Industries"
```
