# -*- coding: utf-8 -*-
"""Functionality to parse the career page
TODO use OWAPI instead?
"""

import urllib.request
import urllib.error

from bs4 import BeautifulSoup


URL = "https://playoverwatch.com/en-us/career/pc/{:s}"


def parse_player(battle_tag):
    try:
        request = urllib.request.Request(URL.format(battle_tag.replace("#", "-")))
        response = urllib.request.urlopen(request)
    except urllib.error.HTTPError:
        print("The HTTP request did not succeed.")
        return None

    soup = BeautifulSoup(response, "html.parser")
    if '"code":404' in soup.contents[0]:
        return None

    player = {
        "rank": int(soup.find(class_="competitive-rank").div.string),
        "battle_tag": battle_tag
    }

    heroes = [option.string.replace(".", "")
              for option in soup.select("#competitive .career-stats-section select option")]
    for index, hero_section in enumerate(soup.select("#competitive .career-stats-section div.row.js-stats")):
        hero = parse_hero(hero_section)
        player[heroes[index]] = hero
    return player


def parse_hero(section):
    hero = {}
    for stat_table in section.select(".card-stat-block table"):
        stat_title = stat_table.find(class_="stat-title").string
        stats = {}
        for row in stat_table.select("tbody tr"):
            name, value = parse_row(row)
            stats[name] = value
        hero[stat_title] = stats
    return hero


def parse_row(row):
    name = row.contents[0].string
    value = row.contents[1].string
    value = format_value(name, value)
    return name, value


def format_value(attribute_name, value):
    if "%" in value:
        return float(value.strip("%")) / 100
    elif "time" in attribute_name.lower():
        return format_time(value)
    return float(value.replace(",", "")) if "." in value else int(value.replace(",", ""))


def format_time(time):
    """formats a given time (%H:%M:%S) to seconds -- considering special case with string in parameter"""
    if time.islower() or time.isupper():
        return float(time.split(" ")[0])  # time contains description (e.g. 2 hours) -> 0 contains number
    return sum(int(x) * 60 ** index for index, x in enumerate(time.split(":")[::-1]))



