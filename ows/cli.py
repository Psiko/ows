#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""The command line interface, can also be used to parse commands from Mumble Moderator"""

import sys
import argparse

from ows import database as db
from ows.update_history import parse_player, update_history


def match_name(user_input, collection, search_element, element_readable):
    matched_names = collection.find({search_element: {'$regex': user_input, '$options': 'i'}})
    names_count = matched_names.count()
    if names_count == 1:
        matched_name = matched_names[0]["_id"]
        print('using {:s} "{:s}"'.format(element_readable, matched_name))
        return matched_name
    else:
        print('{:s} "{:s}" not found'.format(element_readable, user_input))
        if names_count > 1:
            print("Ambiguous choices:")
            for matched_map in matched_names:
                print(matched_map["_id"])


def register_player(arg):
    player = parse_player(battle_tag=arg)
    if player is None:
        print('Profile for BattleTag "{:s}" not found.'.format(arg))
    else:
        db.profile.update({"battle_tag": arg}, player, True)
        print('Profile for BattleTag "{:s}" successfully saved.'.format(arg))


def do_command(argv=sys.argv[1:]):
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest="subcmd")
    update_history_parser = subparsers.add_parser("update_history")
    update_history_parser.add_argument("-m", "--map", help="Manually select a map that was played.")
    register_player_parser = subparsers.add_parser("register_player")
    register_player_parser.add_argument("battle_tag")
    # foo_parser.add_argument("-a", "--all", help="Show all games", action="store_",)
    args = parser.parse_args(argv)
    subcmd = args.subcmd
    if subcmd == "update_history":
        map_name = None
        if args.map:
            map_name = match_name(args.map, db.maps, "_id", "map")
        update_history(map_name)
    elif subcmd == "register_player":
        register_player(args.battle_tag)
    else:
        print("unknown subcommand")


if __name__ == "__main__":
    do_command()
