# -*- coding: utf-8 -*-
"""Establishes the database connection"""

from pymongo import MongoClient

from ows.maps import maps, categories


def _init_db(db):
    db.profiles.create_index("battle_tag", unique=True)
    db.history.create_index([("game_index", 1), ("battle_tag", 1)], unique=True)
    for map_name in maps:
        if db.maps.find({"_id": map_name}).limit(1).count() == 0:
            db.maps.insert_one({"_id": map_name, "category": categories[maps[map_name]]})


client = MongoClient("localhost", 27017)
database = client.overwatch
_init_db(database)
