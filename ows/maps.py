# -*- coding: utf-8 -*-
"""Static maps data, there is currently no source to update this dynamically"""

categories = {0: "Assault", 1: "Escort", 2: "Hybrid", 3: "Control"}

maps = {
    "Hanamura": 0,
    "Horizon Lunar Colony": 0,
    "Temple of Anubis": 0,
    "Volskaya Industries": 0,
    "Dorado": 1,
    "Junkertown": 1,
    "Route 66": 1,
    "Watchpoint: Gibraltar": 1,
    "Blizzard World": 2,
    "Eichenwalde": 2,
    "Hollywood": 2,
    "King's Row": 2,
    "Numbani": 2,
    "Ilios": 3,
    "Lijiang Tower": 3,
    "Nepal": 3,
    "Oasis": 3
}