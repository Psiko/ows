# -*- coding: utf-8 -*-
"""Update logic to separate game stats and save them"""

from datetime import datetime

from pymongo.errors import DuplicateKeyError

from ows import database as db
from ows.parsing import parse_player


def calculate_game(old_profile, updated_profile, old_game_index, map_name):
    won = updated_profile["ALL HEROES"]["Game"]["Games Won"] - old_profile["ALL HEROES"]["Game"]["Games Won"]
    lost = updated_profile["ALL HEROES"]["Game"]["Games Lost"] - old_profile["ALL HEROES"]["Game"]["Games Lost"]
    win = (1 + won - lost) / 2  # considering a tie
    game = {
        "battle_tag": old_profile["battle_tag"],
        "game_index": old_game_index + 1,
        "sr_change": updated_profile["rank"] - old_profile["rank"],
        "win": win,
        "stats": {},
        "map": map_name,
        "timestamp": datetime.utcnow()
    }

    for hero, stats in updated_profile.items():
        if type(stats) is dict:  # ignore non dictionary attributes, e.g. battle_tag
            hero_stats = get_hero_stats(old_profile, stats, hero)
            if hero_stats is not None:  # if hero was played
                game["stats"][hero] = hero_stats
    return game


def get_hero_stats(old_profile, updated_stats, hero):
    if hero not in old_profile:
        return updated_stats
    if updated_stats["Game"]["Games Played"] - old_profile[hero]["Game"]["Games Played"] < 1:
        return None
    hero_stats = {}
    relevant_categories = ["Combat", "Assists", "Match Awards", "Miscellaneous", "Hero Specific"]
    for category in relevant_categories:
        if category not in updated_stats:
            continue
        category_stats = {}
        for key, value in updated_stats[category].items():
            category_stats[key] =\
                value - old_profile[hero][category][key] if key in old_profile[hero][category] else value
        hero_stats[category] = category_stats
    return hero_stats


def update_history(map_name):
    for profile in db.profile.find():
        game_index = profile["ALL HEROES"]["Game"]["Games Played"]
        updated_profile = parse_player(profile["battle_tag"])
        games_played = updated_profile["ALL HEROES"]["Game"]["Games Played"] - game_index
        if games_played == 0:
            print('No updates for profile "{}" monitored -- {} games played so far.'
                  .format(profile["battle_tag"], game_index))
        elif games_played > 1:
            print('Monitored {} new games for profile "{}", only updating profile.'
                  .format(games_played, profile["battle_tag"]))
            db.profile.update({"battle_tag": profile["battle_tag"]}, updated_profile)
        else:
            game = calculate_game(profile, updated_profile, game_index, map_name)
            try:
                db.history.insert_one(game)
                print('Successfully updated match history for profile "{}".'.format(profile["battle_tag"]))
            except DuplicateKeyError:
                print("Game already in database -- error highly possible.")
            db.history.update({"battle_tag": profile["battle_tag"]}, updated_profile, True)




